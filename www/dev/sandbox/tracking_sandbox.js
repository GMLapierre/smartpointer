/**
 * Created by guillaumemartin-lapierre on 2017-02-22.
 */

define(["../jsfeat", "../tracking"], function () {

    function convert_to_gray() {
        var canvas = document.getElementById('initCanvas'),
            context = canvas.getContext('2d'),
            image = new Image();
        image.src = './images/image_test_1.jpg';

        // Helper function
        function matrix2img(mat) {
            var imageData = new ImageData(mat.cols, mat.rows);

            var dataU32 = new Uint32Array(imageData.data.buffer);
            var alpha = (0xff << 24);
            var i = mat.cols * mat.rows, pix = 0;
            while (--i >= 0) {
                pix = mat.data[i];
                dataU32[i] = alpha | (pix << 16) | (pix << 8) | pix;
            }
            return imageData;
        }

        // Helper function
        function drawMat(mat, parent, cnvStyle) {
            var cnv = document.createElement('canvas');
            if (typeof cnvStyle !== 'undefined')
                cnv.className = cnvStyle;
            cnv.width = mat.cols;
            cnv.height = mat.rows;

            parent.appendChild(cnv);
            var ctx = cnv.getContext('2d');

            if (mat.channel === jsfeat.C1_t) {
                ctx.putImageData(matrix2img(mat), 0, 0);
            } else if (mat.channel === jsfeat.C3_t || mat.channel === jsfeat.C4_t) {
                ctx.putImageData(matrix2imgInColor(mat), 0, 0);
            }
            return ctx.getImageData(0, 0, mat.cols, mat.rows);
        }

        image.onload = function () {
            var cols = image.width;
            var rows = image.height;
            canvas.width = cols;
            canvas.height = rows;
            context.drawImage(image, 0, 0, image.width, image.height, 0, 0, cols, rows);
            var imageData = context.getImageData(0, 0, cols, rows);
            // jsfeat -> tracking.js
            var dataBuffer = new jsfeat.data_t(cols * rows, imageData.data.buffer);
            var mat = new jsfeat.matrix_t(cols, rows, jsfeat.U8C3_t, dataBuffer);

            var gray = tracking.Image.grayscale(mat.data, cols, rows, true);
            context.putImageData(new ImageData(gray, cols, rows), 0, 0);
            // tracking.js -> jsfeat
            var buf = new Array(gray.length / 4);
            for (var i = 0, j = 0; i < gray.length; i += 4, ++j) {
                buf[j] = gray[i];
            }
            var matGray = new jsfeat.matrix_t(cols, rows, jsfeat.U8C1_t,
                new jsfeat.data_t(cols * rows, buf));
            drawMat(matGray, document.getElementById('images'), 'canvas-img');
        }
    }
    return {
        convert_to_gray: convert_to_gray
    };
});