/**
 * Created by guillaumemartin-lapierre on 2017-02-19.
 */

define(["../jsfeat", "print"], function (print) {

    // Helper function
    function normalizeAndDraw(mat, parent, cnvStyle) {
        var max = Math.max.apply(null, mat.data);
        for (var i = 0; i < mat.data.length; i++) {
            mat.data[i] = mat.data[i] * 255 / max;
        }
        drawMat(mat, parent, cnvStyle);
    }

    // Helper function
    function matrixFromCtx(ctx, cols, rows) {
        var imageData = ctx.getImageData(0, 0, cols, rows);
        var dataBuffer = new jsfeat.data_t(cols * rows, imageData.data.buffer);

        return new jsfeat.matrix_t(cols, rows, jsfeat.U8_t | jsfeat.C4_t, dataBuffer);
    }

    // Helper function
    function drawMat(mat, parent, cnvStyle) {
        var cnv = document.createElement('canvas');
        if (typeof cnvStyle !== 'undefined')
            cnv.className = cnvStyle;
        cnv.width = mat.cols;
        cnv.height = mat.rows;

        parent.appendChild(cnv);
        var ctx = cnv.getContext('2d');

        if (mat.channel === jsfeat.C1_t) {
            ctx.putImageData(matrix2img(mat), 0, 0);
        } else if (mat.channel === jsfeat.C3_t || mat.channel === jsfeat.C4_t) {
            ctx.putImageData(matrix2imgInColor(mat), 0, 0);
        }
        return ctx.getImageData(0, 0, mat.cols, mat.rows);
    }

    // Helper function
    function matrix2img(mat) {
        var imageData = new ImageData(mat.cols, mat.rows);

        var dataU32 = new Uint32Array(imageData.data.buffer);
        var alpha = (0xff << 24);
        var i = mat.cols * mat.rows, pix = 0;
        while (--i >= 0) {
            pix = mat.data[i];
            dataU32[i] = alpha | (pix << 16) | (pix << 8) | pix;
        }
        return imageData;
    }

    // Helper function
    function matrix2imgInColor(mat) {
        var data = new Uint8ClampedArray(mat.data);
        return new ImageData(data, mat.cols, mat.rows);
    }

    // Helper function
    function matrixDemo() {
        var matrix = new jsfeat.matrix_t(3, 3, jsfeat.U8_t | jsfeat.C1_t);
        // matrix.data[1] = 1;
        // matrix.data[5] = 2;
        // matrix.data[7] = 1;


        for (var i = 0; i < matrix.rows; ++i) {
            console.log(matrix);
            var start = i * matrix.cols;
            console.log(matrix.data.subarray(start, start + matrix.cols));
        }
    }

    // Common way to display an image to a canvas
    function colorImageToGray() {
        var canvas = document.getElementById('initCanvas'),
            context = canvas.getContext('2d'),
            image = new Image();
            image.src = './images/image_test_1.jpg';

            image.onload = function () {
                // Draw image to canvas
                const cols = image.width;
                const rows = image.height;
                canvas.width = cols;
                canvas.height = rows;
                context.drawImage(image, 0, 0, cols, rows);

                // Load image to matrix
                var imageDataMatrix = context.getImageData(0, 0, cols, rows);
                var dataBuffer = new jsfeat.data_t(cols * rows, imageDataMatrix.data.buffer);
                var mat = new jsfeat.matrix_t(cols, rows, jsfeat.U8_t | jsfeat.C4_t, dataBuffer);

                // Convert image to grayscale
                var gray = new jsfeat.matrix_t(mat.cols, mat.rows, jsfeat.U8_t | jsfeat.C1_t);
                jsfeat.imgproc.grayscale(mat.data, mat.cols, mat.rows, gray);

                // Clear image for proof of concept
                context.clearRect(0, 0, cols, rows);

                // Modify existing colored matrix to alpha only components (to make the image gray)
                var imageData = new ImageData(gray.cols, gray.rows);
                var data = new Uint32Array(imageData.data.buffer);
                var alpha = (0xff << 24);
                var i = gray.cols * gray.rows, pix = 0;
                while (--i >= 0) {
                    pix = gray.data[i];
                    data[i] = alpha | (pix << 16) | (pix << 8) | pix;
                }

                // Add image data to canvas
                context.putImageData(imageData, 0, 0);
            };
    }

    function sorting() {

        // Define a sorting function
        var compareFunc = function (a, b) {
            return a > b;

};

        var canvas = document.getElementById('initCanvas'),
        context = canvas.getContext('2d'),
        image = new Image();
        image.src = './images/image_test_1.jpg';

        image.onload = function () {
            // Draw image to canvas
            const cols = image.width;
            const rows = image.height;
            canvas.width = cols;
            canvas.height = rows;
            context.drawImage(image, 0, 0, cols, rows);

            // Load image to matrix
            var imageDataMatrix = context.getImageData(0, 0, cols, rows);
            var dataBuffer = new jsfeat.data_t(cols * rows, imageDataMatrix.data.buffer);
            var mat = new jsfeat.matrix_t(cols, rows, jsfeat.U8_t | jsfeat.C4_t, dataBuffer);

            // Sort the image
            var length = mat.data.length;
            jsfeat.math.qsort(mat.data, 0, length - 1, compareFunc);
            //length / 3 * 2

            // Convert image to grayscale
            var gray = new jsfeat.matrix_t(mat.cols, mat.rows, jsfeat.U8_t | jsfeat.C1_t);
            jsfeat.imgproc.grayscale(mat.data, mat.cols, mat.rows, gray);


            // Clear image for proof of concept
            context.clearRect(0, 0, cols, rows);

            // Modify existing colored matrix to alpha only components (to make the image gray)
            var imageData = new ImageData(gray.cols, gray.rows);
            var data = new Uint32Array(imageData.data.buffer);
            var alpha = (0xff << 24);
            var i = gray.cols * gray.rows, pix = 0;
            while (--i >= 0) {
                pix = gray.data[i];
                data[i] = alpha | (pix << 16) | (pix << 8) | pix;
            }

            // Add image data to canvas
            context.putImageData(imageData, 0, 0);
        };
    }

    function homography() {
        var canvas = document.getElementById('initCanvas'),
            context = canvas.getContext('2d'),
            image = new Image();
            image.src = './images/laser_pointer.jpg';

        function getRectified(mat) {
            var imgRectified = new jsfeat.matrix_t(mat.cols, mat.rows, jsfeat.U8_t | jsfeat.C1_t);
            var transform = new jsfeat.matrix_t(3, 3, jsfeat.F32_t | jsfeat.C1_t);
            jsfeat.math.perspective_4point_transform(transform,
                0, 0, 0, 100, // first pair x1_src, y1_src, x1_dst, y1_dst
                640, 0, 640, 0, // x2_src, y2_src, x2_dst, y2_dst and so on.
                640, 480, 640, 480,
                0, 480, 120, 480);
            jsfeat.matmath.invert_3x3(transform, transform);
            jsfeat.imgproc.warp_perspective(mat, imgRectified, transform, 255);
            return imgRectified;
        }
        image.onload = function () {
            var cols = image.width;
            var rows = image.height;
            canvas.width = cols;
            canvas.height = rows;
            context.drawImage(image, 0, 0, image.width, image.height, 0, 0, cols, rows);
            var imageData = context.getImageData(0, 0, cols, rows);
            var dataBuffer = new jsfeat.data_t(cols * rows, imageData.data.buffer);
            var mat = new jsfeat.matrix_t(cols, rows, jsfeat.U8_t | jsfeat.C4_t, dataBuffer);
            var gray = new jsfeat.matrix_t(mat.cols, mat.rows, jsfeat.U8_t | jsfeat.C1_t);
            jsfeat.imgproc.grayscale(mat.data, mat.cols, mat.rows, gray);
            var imgRectified = getRectified(gray);
            drawMat(imgRectified, document.getElementById('images'), 'canvas-img');
        }
    }

    function colorThreshold() {
        var canvas = document.getElementById('initCanvas'),
            context = canvas.getContext('2d'),
            image = new Image();
        image.src = './images/image_test_1.jpg';

        image.onload = function () {
            // Draw image to canvas
            const cols = image.width;
            const rows = image.height;
            canvas.width = cols;
            canvas.height = rows;
            context.drawImage(image, 0, 0, cols, rows);

            var img_data = context.getImageData(0, 0, cols, rows);

            var data = img_data.data;
            var threshold = 140;

            for (var i = 0; i < data.length; i += 4) {
                var red = data[i]; // red
                var green = data[i + 1]; // green
                var blue = data[i + 2]; // blue
                // i+3 is alpha (the fourth element)
                data[i] = 0;
                data[i + 1] = 0;
                data[i + 2] = 0;
                //temp = Math.max(red, green - blue);

                if (blue > threshold) {
                    //console.log(i % cols - 1, Math.floor(i / cols));
                    data[i + 3] = 255;
                } else {
                    data[i + 3] = 0;
                }

            }

            context.putImageData(img_data, 0, 0);

        };
    }

    function gaussianBlur() {
        var canvas = document.getElementById('initCanvas'),
            context = canvas.getContext('2d'),
            image = new Image();
        image.src = './images/laser_pointer_2.jpg';

        image.onload = function () {
            var cols = image.width;
            var rows = image.height;
            canvas.width = cols;
            canvas.height = rows;

            context.drawImage(image, 0, 0, image.width, image.height, 0, 0, cols, rows);

            var imageData = context.getImageData(0, 0, cols, rows);
            var dataBuffer = new jsfeat.data_t(cols * rows, imageData.data.buffer);

            var mat = new jsfeat.matrix_t(cols, rows, jsfeat.U8_t | jsfeat.C4_t, dataBuffer);
            var gray = new jsfeat.matrix_t(mat.cols, mat.rows, jsfeat.U8_t | jsfeat.C1_t);
            var blur = new jsfeat.matrix_t(mat.cols, mat.rows, jsfeat.U8_t | jsfeat.C1_t);
            jsfeat.imgproc.grayscale(mat.data, mat.cols, mat.rows, gray);

            var kernel_size = 10, sigma = 0, kernelArray = [], dataType = jsfeat.F32_t;

            jsfeat.imgproc.gaussian_blur(gray, blur, kernel_size, sigma = 0);

            drawMat(blur, document.getElementById('images'), 'canvas-img');
        }
    }

    return {
        matrixDemo: matrixDemo,
        colorImageToGray: colorImageToGray,
        sorting: sorting,
        homography: homography,
        colorThreshold: colorThreshold,
        gaussianBlur: gaussianBlur
    };
});