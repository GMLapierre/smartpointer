/**
 * Created by guillaumemartin-lapierre on 2017-03-17.
 */
define("main", ["../modules/HTMLFactory", "../modules/WebRTC", "../modules/Filters",
        "../modules/DiffCamEngine",  "../modules/Features", "../lib/easeljs", "../lib/html5-canvas-bar-graph"],
    function (HTMLFactory, WebRTC, Filters, DiffCamEngine, Features) {

        init();

        // Histograms
        let red_graph;
        let green_graph;
        let blue_graph;

        let r = createEmptyArray(260);
        let g = createEmptyArray(260);
        let b = createEmptyArray(260);

        let arrays = [r, g, b];

        let canvas_stage_width = 640;
        let canvas_stage_height = 480;
        let canvas_motion_width = canvas_stage_width / 1;
        let canvas_motion_height = canvas_stage_height / 1;

        // Main canvas object where images will be displayed
        let stage;

        let html_factory = new HTMLFactory("main-div");

        let bar_graph_canvas_red = html_factory.createCanvas('main-div', canvas_stage_width, canvas_stage_height, 'histogram_red', true);
        let bar_graph_canvas_green = html_factory.createCanvas('main-div', canvas_stage_width, canvas_stage_height, 'histogram_green', true);
        let bar_graph_canvas_blue = html_factory.createCanvas('main-div', canvas_stage_width, canvas_stage_height, 'histogram_blue', true);

        let coords = html_factory.createDiv("main-div", "(x, y)", false, true);

        let motion_canvas = html_factory.createCanvas("main-div", canvas_motion_width, canvas_motion_height, 'motion_canvas', true);

        let stage_canvas = html_factory.createCanvas("main-div", canvas_stage_width, canvas_stage_height, 'stage_canvas', false);

        let video = html_factory.createVideo("main-div", canvas_motion_width, canvas_motion_height, 'video', false);

        let video_canvas = html_factory.createCanvas("main-div", canvas_stage_width, canvas_stage_height, 'video_canvas', false);

        let params = {
            includeMotionPixels: true,
            includeMotionBox: false,
            pixelDiffThreshold: 32,
            motionCanvas: motion_canvas,
            video: video,
            diffWidth: canvas_motion_width,
            diffHeight: canvas_motion_height,
            initSuccessCallback: initSuccess,
            initErrorCallback: initError,
            captureCallback: capture};

        console.log("Diff cam engine params: ", params);

        setInterval(update, 100);

        //Set up a requestAnimationFrame loop
        function update () {

            video_canvas.getContext('2d').drawImage(video, 0, 0);

            if (motion_canvas !== undefined) {
                // Get image data and store create histogram
                populateArrays(getImageData(video_canvas), arrays);
                updateArrays(arrays);
            }
        }

        DiffCamEngine.init(params);

        function updateArrays(arrays) {
            let graphs = [red_graph, green_graph, blue_graph];

            for (let i = 0; i < arrays.length; i++) {
                graphs[i].update(arrays[i]);
            }
        }

        function getImageData(canvas) {
            return canvas.getContext("2d").getImageData(0, 0, canvas.width, canvas.height);
        }

        function createEmptyArray(size=255) {
            let number_arr = [];
            for (let i = 0; i < size; i++) {
                number_arr.push(i);
            }
            return number_arr;
        }

        function populateArrays(imageData, arrays) {
            // reset the histogram arrays
            resetArrays(arrays);
            // Parses the imageData array and increments every rgb array
            for (let i = 0; i < imageData.data.length; i += 4) {
                //console.log([imageData.data[i]]);
                arrays[0][imageData.data[i]] += 1;
                arrays[1][imageData.data[i + 1]] += 1;
                arrays[2][imageData.data[i + 2]] += 1;
            }
        }

        function resetArrays(arrays) {
            for (let j = 0; j < arrays.length; j++) {
                for (let i = 0; i < arrays[j].length; i++) {
                    arrays[j][i] = 0;
                }
            }
        }

        function handlePayload(motionPixels) {

            let x = motionPixels.length;
            console.log(motionPixels);
            let y = motionPixels[x - 1].length;
            console.log(motionPixels[x - 1]);
            if (coords) {
                coords.innerHTML = x.toString() + " " + y.toString();
            }
            if (stage && circle) {
                moveCircle(x, y);
                stage.update();
            }
        }

        function moveCircle(x, y) {
            if (circle) {
                circle.x = x;
                circle.y = y;
                console.log(x, y);
                coords.innerHTML = "X: " + x.toString() + ", Y: " + y.toString();
            }
        }

        function initSuccess() {
            DiffCamEngine.start();
        }

        function initError() {
            alert('Something went wrong.');
        }

        function capture(imageData) {
            //DiffCamEngine.setPixelDiffThreshold(DiffCamEngine.getLuminosity());

            if (imageData.motionPixels.length > 0) {
                blobCalculator(imageData.imageData);
                handlePayload(imageData.motionPixels);

            } else {
                 if (stage) {
                    stage.clear();
                }
            }
         }

        function initStageCanvas() {
            console.log("Init Stage Canvas");
            stage = new createjs.Stage(stage_canvas);
            circle = new createjs.Shape();
            circle.graphics.beginFill("red").drawCircle(0, 0, 5);
            circle.x = circle.y = 50;

            stage.addChild(circle);
        }

        function initMotionCanvas() {
            console.log("Init Motion Canvas");
        }

        function initWebcamCanvas() {
            console.log("Init Webcam Canvas - TODO");
        }

        function initVideoElement() {
            console.log("Init video element - TODO");
        }

        function initHistogramRed() {
            // Initialises every population to 0
            console.log("Init Histogram");
            let ctx = bar_graph_canvas_red.getContext("2d");

            red_graph = new BarGraph(ctx);
            red_graph.margin = 0;
            red_graph.width = 640;
            red_graph.height = 100;
            red_graph.update(createEmptyArray(260));
        }

        function initHistogramGreen() {
            // Initialises every population to 0
            console.log("Init Histogram");
            let ctx = bar_graph_canvas_green.getContext("2d");

            green_graph = new BarGraph(ctx);
            green_graph.margin = 0;
            green_graph.width = 640;
            green_graph.height = 100;
            green_graph.update(createEmptyArray(260));
        }

        function initHistogramBlue() {
            // Initialises every population to 0
            console.log("Init Histogram");
            let ctx = bar_graph_canvas_blue.getContext("2d");

            blue_graph = new BarGraph(ctx);
            blue_graph.margin = 0;
            blue_graph.width = 640;
            blue_graph.height = 100;
            blue_graph.update(createEmptyArray(260));
        }

        function blobCalculator(image_data, threshold=32) {
            let x_coord = 0;
            let y_coord = 0;
            let size = 0;
            let coords = {
                x: x_coord,
                y: y_coord
            };
            // for (let i = 0; i <image_data.length; i += 4) {
            //     if (image_data[i])
            // }
            // console.log(image_data);
            return coords;
        }

        function init() {
            console.log("main.js loaded correctly");
            let features = new Features();
            features.showFeatures();
            let observer = new MutationObserver(function(mutations){
                for (let i=0; i < mutations.length; i++){
                    for (let j=0; j < mutations[i].addedNodes.length; j++){
                        checkNode(mutations[i].addedNodes[j]);
                    }
                }
            });
            checkNode = function(addedNode) {
                if (addedNode.nodeType === 1){
                    if (addedNode.matches('#stage_canvas')){
                        console.log("Stage canvas element added");
                        initStageCanvas();
                    }
                    if (addedNode.matches('#motion_canvas')){
                        console.log("Motion canvas element added");
                        initMotionCanvas();
                    }
                    if (addedNode.matches('#webcam_canvas')){
                        console.log("Webcam_canvas element added");
                        initWebcamCanvas();
                    }
                    if (addedNode.matches('#webcam')){
                        console.log("Video element added");
                        initVideoElement();
                    }
                    if (addedNode.matches('#histogram_red')){
                        console.log("Histogram element added");
                        initHistogramRed();
                    }
                    if (addedNode.matches('#histogram_green')){
                        console.log("Histogram element added");
                        initHistogramGreen();
                    }
                    if (addedNode.matches('#histogram_blue')){
                        console.log("Histogram element added");
                        initHistogramBlue();
                    }

                }
            };
            observer.observe(document.body, {
                childList: true,
                subtree: true
            });
        }

});
