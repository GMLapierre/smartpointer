/**
 * Created by guillaumemartin-lapierre on 2017-03-19.
 */

define(function() {
    // Start with the constructor
    function HTMLFactory(div_id) {
        this.main_div = document.createElement('div');
        this.main_div.id = div_id;
        document.body.appendChild(this.main_div);
    }

    // Appends a canvas to the parent node
    HTMLFactory.prototype.createCanvas = function(parentNode, width, height, id=false, add_to_DOM=false) {
        let canvas = document.createElement('canvas');
        canvas.height = height;
        canvas.width = width;
        if (id) {
            canvas.id = id;
        }

        if (add_to_DOM) {
            document.getElementById(parentNode).appendChild(canvas);
            return canvas;
        }
        return canvas;

    };

    HTMLFactory.prototype.createDiv = function(parentNode, text=false, id=false, add_to_DOM=false) {
        let div = document.createElement('div');
        if (text) {
            div.innerHTML = text;
        }
        div.innerHTML = text;
        if (id) {
            div.id = id;
        }

        if (add_to_DOM) {
            document.getElementById(parentNode).appendChild(div);
            return div;
        }
        return div;

    };

    HTMLFactory.prototype.createVideo = function(parentNode, width, height, id=false, add_to_DOM=false) {
        let video = document.createElement('video');
        if (id) {
            video.id = id;
        }

        if (add_to_DOM) {
            document.getElementById(parentNode).appendChild(video);
            return video;
        }
        return video;
    };

    // And now return the constructor function
    return HTMLFactory;
});