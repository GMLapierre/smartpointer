/**
 * Created by guillaumemartin-lapierre on 2017-03-19.
 */
define(function() {
    function Filters() {
        console.log("Creating filters");
    }


    // Returns an image data object
    Filters.prototype.grayscale = function(image_data, args) {
        console.log("Applying grayscale filter");
        for (let i = 0; i < image_data.length; i += 4) {
            let r = image_data[i];
            let g = image_data[i + 1];
            let b = image_data[i + 2];
            // CIE luminance for the RGB
            // The human eye is bad at seeing red and blue, so we de-emphasize them.
            let v = 0.2126 * r + 0.7152 * g + 0.0722 * b;
            image_data[i] = image_data[i + 1] = image_data[i + 2] = v
        }
        return image_data;
    };

    Filters.prototype.threshold = function(image_data, threshold) {
        for (let i = 0; i < image_data.length; i += 4) {
            let r = image_data[i];
            let g = image_data[i + 1];
            let b = image_data[i + 2];
            let v = (0.2126 * r + 0.7152 * g + 0.0722 * b >= threshold) ? 255 : 0;
            image_data[i] = image_data[i + 1] = image_data[i + 2] = v
        }
        return image_data;
    };

    // And now return the constructor function
    return Filters;
});