/**
 * Created by guillaumemartin-lapierre on 2017-03-19.
 */
define(function() {
    // Must have a video element
    let video = document.getElementById('webcam');

    function WebRTC(_video) {
        console.log('Creating WebRTC object', _video);
        this.video = document.getElementById('webcam');
        this.constraints = {
            audio: false,
            video: {}// width: canvas.width, height: canvas.height }
        };
        console.log(this.video);
    }

    WebRTC.prototype.startVideo = function() {
        if(navigator.getUserMedia) {
            navigator.getUserMedia(this.constraints, _getUserMediaSuccess, _getUserMediaError);
        } else {
            alert('Your browser does not support getUserMedia API');
        }

    };

    function _getUserMediaSuccess(stream) {
        console.log(this.video);
        video.src = window.URL.createObjectURL(stream);
    }

    function _getUserMediaError(stream) {
        console.log(this.video);
        video.src = window.URL.createObjectURL(stream);
    }
    // And now return the constructor function
    return WebRTC;
});