/**
 * Created by guillaumemartin-lapierre on 2017-03-30.
 */


define(function() {
    // Must have a video element
    function Features(name) {
        console.log("The features of this application are: ");
        this.feature1 =  "Feature1: Moves a red dot on a stage canvas depending on motion from camera"
    }

    //
    Features.prototype.showFeatures = function() {
       console.log(this.feature1);
    };

    // And now return the constructor function
    return Features;
});