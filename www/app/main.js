/**
 * Created by guillaumemartin-lapierre on 2017-03-17.
 */

// require([
//     'dat/gui/GUI'
// ], function(GUI) {
//     window.dat = {
//         GUI: GUI
//     };
// });

define("main", ["../modules/HTMLFactory", "../modules/WebRTC", "../modules/Filters",
        "../modules/DiffCamEngine",  "../modules/Features", "../lib/easeljs", "../lib/html5-canvas-bar-graph"],
    function (HTMLFactory, WebRTC, Filters, DiffCamEngine, Features) {
        init();

        let canvas_stage_width = 320;
        let canvas_stage_height = 240;
        let canvas_motion_width = canvas_stage_width / 1;
        let canvas_motion_height = canvas_stage_height / 1;
        let current_slide = 0;
        // Histograms
        let red_graph;
        let green_graph;
        let blue_graph;
        let r = createEmptyArray(260);
        let g = createEmptyArray(260);
        let b = createEmptyArray(260);

        let arrays = [r, g, b];

        let current = 0;

        let background_0 = new createjs.Bitmap("../ressources/slides/slide0.jpg");
        let background_1 = new createjs.Bitmap("../ressources/slides/slide1.jpg");
        let background_2 = new createjs.Bitmap("../ressources/slides/slide2.jpg");
        let background_3 = new createjs.Bitmap("../ressources/slides/slide3.jpg");

        let slides = [background_0, background_1, background_2, background_3];

        let html_factory = new HTMLFactory("main-div");

        let bar_graph_canvas_red = html_factory.createCanvas('main-div', canvas_stage_width, canvas_stage_height, 'histogram_red', true);
        let bar_graph_canvas_green = html_factory.createCanvas('main-div', canvas_stage_width, canvas_stage_height, 'histogram_green', true);
        let bar_graph_canvas_blue = html_factory.createCanvas('main-div', canvas_stage_width, canvas_stage_height, 'histogram_blue', true);

        // Main canvas object where images will be displayed
        let stage;

        let coords = html_factory.createDiv("main-div", "(x, y)", false, true);

        let stage_canvas = html_factory.createCanvas("main-div", canvas_stage_width, canvas_stage_height, 'stage_canvas', true);

        let video = html_factory.createVideo("main-div", canvas_motion_width, canvas_motion_height, 'video', false);

        let video_canvas = html_factory.createCanvas("main-div", canvas_stage_width, canvas_stage_height, 'video_canvas', false);

        let motion_canvas = html_factory.createCanvas("main-div", canvas_motion_width, canvas_motion_height, 'motion_canvas', true);

        let params = {
            includeMotionPixels: true,
            includeMotionBox: false,
            pixelDiffThreshold: 44,
            motionCanvas: motion_canvas,
            video: video,
            diffWidth: canvas_motion_width,
            diffHeight: canvas_motion_height,
            initSuccessCallback: initSuccess,
            initErrorCallback: initError,
            captureCallback: capture};

        DiffCamEngine.init(params);

        setInterval(update, 100);

        //Set up a requestAnimationFrame loop
        function update () {
            video_canvas.getContext('2d').drawImage(video, 0, 0);

            if (motion_canvas !== undefined) {
                // Get image data and store create histogram
                populateArrays(getImageData(video_canvas), arrays);
                updateArrays(arrays);
            }
        }

        function updateArrays(arrays) {
            let graphs = [red_graph, green_graph, blue_graph];

            for (let i = 0; i < arrays.length; i++) {
                graphs[i].update(arrays[i]);
            }
        }

        function getImageData(canvas) {
            return canvas.getContext("2d").getImageData(0, 0, canvas.width, canvas.height);
        }

        function createEmptyArray(size=255) {
            let number_arr = [];
            for (let i = 0; i < size; i++) {
                number_arr.push(i);
            }
            return number_arr;
        }

        function populateArrays(imageData, arrays) {
            // reset the histogram arrays
            resetArrays(arrays);
            // Parses the imageData array and increments every rgb array
            for (let i = 0; i < imageData.data.length; i += 4) {
                //console.log([imageData.data[i]]);
                arrays[0][imageData.data[i]] += 1;
                arrays[1][imageData.data[i + 1]] += 1;
                arrays[2][imageData.data[i + 2]] += 1;
            }
        }

        function resetArrays(arrays) {
            for (let j = 0; j < arrays.length; j++) {
                for (let i = 0; i < arrays[j].length; i++) {
                    arrays[j][i] = 0;
                }
            }
        }

        function handleMove(score, motionPixels) {
            let x = motionPixels.length;
            let y = motionPixels[x - 1].length;
            console.log(x, y);
            coords.innerHTML = "X: " + x.toString() + ", Y: " + y.toString();
            circle.x = x;
            circle.y = y;
            offset = 80;

            console.log(left_button.x, left_button.y);
            if (circle.x > right_button.x){
                console.log("GO RIGHT!");
                stage.removeChild(slides[current]);
                current += 1;
                if (current > 3) {
                    current = 3;
                }
                stage.addChild(slides[current]);

            }
            if (circle.x < left_button.x + 20 ) {
                console.log("GO LEFT!");
                stage.removeChild(slides[current]);
                current -= 1;
                if (current < 0) {
                    current = 0;
                }
                stage.addChild(slides[current]);
            }
            stage.update();
        }

        function initSuccess() {
            DiffCamEngine.start();
        }

        function initError() {
            alert('Something went wrong.');
        }

        function capture(payload) {
            if (payload.motionPixels.length > 0) {
                stage.addChild(circle);
                handleMove(coords, payload.motionPixels);
            } else {
                stage.removeChild(circle);
            }
        }

        function initStageCanvas() {
            console.log("InitStage");
            stage = new createjs.Stage(stage_canvas);

            circle = new createjs.Shape();
            circle.graphics.beginFill("red").drawCircle(0, 0, 10);
            circle.x = circle.y = 50;

            right_button = new createjs.Shape();
            right_button.graphics.beginFill("gray").drawRoundRect(0, 0, 20, 20, 1);
            right_button.x = canvas_stage_width - 20;
            right_button.y = canvas_stage_height/2;

            left_button = new createjs.Shape();
            left_button.graphics.beginFill("gray").drawRoundRect(0, 0, 20, 20, 1);
            left_button.x = 0;
            left_button.y = canvas_stage_height/2;

            stage.addChild(background_0);
            stage.addChild(circle);
            //stage.addChild(right_button);
            //stage.addChild(left_button);

            stage.update();
        }

        function initMotionCanvas() {
            console.log("Init Motion Canvas");
        }

        function initHistogramRed() {
            // Initialises every population to 0
            console.log("Init Histogram");
            let ctx = bar_graph_canvas_red.getContext("2d");

            red_graph = new BarGraph(ctx);
            red_graph.margin = 0;
            red_graph.width = (canvas_stage_width / 3) * 2;
            red_graph.height = (canvas_stage_height / 3) * 2;
            red_graph.update(createEmptyArray(260));
        }

        function initHistogramGreen() {
            // Initialises every population to 0
            console.log("Init Histogram");
            let ctx = bar_graph_canvas_green.getContext("2d");

            green_graph = new BarGraph(ctx);
            green_graph.margin = 0;
            green_graph.width = (canvas_stage_width / 3) * 2;
            green_graph.height = (canvas_stage_height / 3) * 2;
            green_graph.update(createEmptyArray(260));
        }

        function initHistogramBlue() {
            // Initialises every population to 0
            console.log("Init Histogram");
            let ctx = bar_graph_canvas_blue.getContext("2d");

            blue_graph = new BarGraph(ctx);
            blue_graph.margin = 0;
            blue_graph.width = (canvas_stage_width / 3) * 2;
            blue_graph.height = (canvas_stage_height / 3) * 2;
            blue_graph.update(createEmptyArray(260));
        }

        function init() {
            console.log("main.js loaded correctly");
            let features = new Features();
            features.showFeatures();
            let observer = new MutationObserver(function(mutations){
                for (let i=0; i < mutations.length; i++){
                    for (let j=0; j < mutations[i].addedNodes.length; j++){
                        checkNode(mutations[i].addedNodes[j]);
                    }
                }
            });
            checkNode = function(addedNode) {
                if (addedNode.nodeType === 1){
                    if (addedNode.matches('#stage_canvas')){
                        console.log("Stage canvas element added");
                        initStageCanvas();
                    }
                    if (addedNode.matches('#motion_canvas')){
                        console.log("Motion canvas element added");
                        initMotionCanvas();
                    }
                    if (addedNode.matches('#webcam_canvas')){
                        console.log("Webcam_canvas element added");
                        initWebcamCanvas();
                    }
                    if (addedNode.matches('#webcam')){
                        console.log("Video element added");
                        initVideoElement();
                    }
                    if (addedNode.matches('#histogram_red')){
                        console.log("Histogram element added");
                        initHistogramRed();
                    }
                    if (addedNode.matches('#histogram_green')){
                        console.log("Histogram element added");
                        initHistogramGreen();
                    }
                    if (addedNode.matches('#histogram_blue')){
                        console.log("Histogram element added");
                        initHistogramBlue();
                    }
                }
            };
            observer.observe(document.body, {
                childList: true,
                subtree: true
            });
        }

        function delay(ms) {
            ms += new Date().getTime();
            while (new Date() < ms){}
        }
    });
