/**
 * Created by guillaumemartin-lapierre on 2017-03-17.
 */

requirejs.config({
    baseUrl: 'lib',
    paths: {
        app: './dev',
        'dat-gui': './lib/dat.gui'
    },
    shim : {
        "bootstrap" : { "deps" :['jquery'] }
    },
});

// Start loading the main app file. Put all of
// your application logic in there.
requirejs(['dev/main']);


