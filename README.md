
This web project has the following setup:

* www/ - the web assets for the project
    * index.html - the entry point into the app
    * dev.html - the entry point for the dev app
    * app.js - the top-level config script used by index.html
    * dev.js - the top-level config script used by dev.html
    * app/ - the directory to store project-specific scripts.
    * lib/ - the directory to hold third party scripts.
    * modules/ - custom made class for the project
* tools/ - the build tools to optimize the project.

Workflow:

A functional version of the application lives within the index.html
A list of features is displayed within the console at the application startup

The dev.html file holds the code for future features. Once the features are functional, the entire dev code should copy pasted in the index.html domain.


To optimize, run:

    node tools/r.js -o tools/build.js

That build command creates an optimized version of the project in a
**www-built** directory. The app.js file will be optimized to include
all of its dependencies.

For more information on the optimizer:
http://requirejs.org/docs/optimization.html

For more information on using requirejs:
http://requirejs.org/docs/api.html
